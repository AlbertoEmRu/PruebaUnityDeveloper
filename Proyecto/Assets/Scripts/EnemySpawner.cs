﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {
	public static EnemySpawner instance;
	//valores de enemigos en el escenario
	//total de enemigos en el nivel
	public int totalEnemies;
	//maximo de enemigos al mismo tiempo
	public int concurrentEnemies;
	//tiempo entre spawn
	public float timeSpawn;
	//objeto a instanciar
	public GameObject enemy;
	//lugares de spawn
	public Vector2[] spawnLocations;
	//numero de enemigos que se instanciaron hasta ahora
	int spawnedEnemies;
	//numero de enemigos en escena
	int currentEnemies;

	void Awake(){
		instance = this;
	}
	//crea los primeros enemigos
	void Start () {
		StartCoroutine ("InitializeEnemies");
	}
	//crea enemigos cada n segundos
	IEnumerator InitializeEnemies(){
		SpawnEnemy ();
		yield return new WaitForSeconds (timeSpawn);
		if (currentEnemies < concurrentEnemies)
			StartCoroutine ("InitializeEnemies");
	}
	//instancia al enemigo
	void SpawnEnemy(){
		currentEnemies += 1;
		spawnedEnemies += 1;
		Vector2 temp = spawnLocations [Random.Range (0, spawnLocations.Length)];
		Instantiate (enemy,new Vector3(temp.x,1,temp.y) , Quaternion.identity);
	}
	//si muere un enemigo crea otro si hace falta
	public void EnemyDead(){
		currentEnemies -= 1;
		if (spawnedEnemies < totalEnemies) {
			StartCoroutine("WaitSpawn");
		}
	}
	IEnumerator WaitSpawn(){
		yield return new WaitForSeconds (timeSpawn);
		SpawnEnemy ();
	}
}
