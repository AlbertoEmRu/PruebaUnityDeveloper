﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstPersonController : MonoBehaviour {
	//velocidad del movimiento
	public float movSpeed =1f;
	int rotSpeed =1;
	CharacterController characterController;
	//camara de personaje
	public Camera mainCamera;
	//zoom
	bool zoom=false;
	bool canZoom=true;
	//furza de disparos
	public float bulletForce;
	//disparador
	public GameObject bulletShooter;
	//pausa
	bool pause=false;


	//obtiene el controlador
	void Start () {
		characterController = GetComponent<CharacterController> ();	
		GameState.instance.play += UnPause;
		GameState.instance.pause += Pause;
	}
	
	//Movimiento para contol de personaje
	void Update () {
		if (!pause) {
			//correr con shift
			if (Input.GetKeyDown (KeyCode.LeftShift) || Input.GetKeyDown (KeyCode.RightShift)) {
				movSpeed *= 2;
			}
			if (Input.GetKeyUp (KeyCode.LeftShift) || Input.GetKeyUp (KeyCode.RightShift)) {
				movSpeed /= 2;
			}
			//movimiento de personaje (calculo)
			Vector3 move = new Vector3 (Input.GetAxis ("Horizontal") * movSpeed,
				              -1f,
				              Input.GetAxis ("Vertical") * movSpeed);

			//rotacion de camara y personaje
			rotSpeed=PlayerProfile.instance.sensibility;
			transform.Rotate (0, Input.GetAxis ("Mouse X") * rotSpeed, 0);
			mainCamera.transform.Rotate (-Input.GetAxis ("Mouse Y") * rotSpeed, 0, 0);
			mainCamera.transform.localRotation = ClampRotationAroundXAxis (mainCamera.transform.localRotation);
			bulletShooter.transform.localRotation = mainCamera.transform.localRotation;
			//manda el movimiento alcontrolador
			move = transform.rotation * move;
			characterController.Move (move * Time.deltaTime);
			//zoom en camara
			if (Input.GetMouseButtonDown (1) && canZoom) {
				canZoom = false;
				if (zoom) {
					float[] range = { 30f, 60f };
					StartCoroutine ("Zoom", range);
					zoom = false;
				} else {
					float[] range = { 60f, 30f };
					StartCoroutine ("Zoom", range);
					zoom = true;
				}
			} 
			if (Input.GetKeyDown (KeyCode.P)) {
				HUDManager.instance.Pause ();

			}
		}

	}
	public void Pause(){
		pause=true;
	}
	public void UnPause(){
		pause = false;
	}
	public bool IsPaused(){
		return pause;
	}
	//colision con bala indica daño a jugador
	void OnCollisionEnter(Collision objCollision){
		if (objCollision.transform.tag == "Bullet") {
			PlayerProfile.instance.PlayerHit ();
		}
	}


	//acota movimiento de camara
	Quaternion ClampRotationAroundXAxis(Quaternion q)
	{
		q.x /= q.w;
		q.y /= q.w;
		q.z /= q.w;
		q.w = 1.0f;
		float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan (q.x);
		angleX = Mathf.Clamp (angleX, -40f, 30f);
		q.x = Mathf.Tan (0.5f * Mathf.Deg2Rad * angleX);
		return q;
	}
	//cambio suvizado en field of view
	IEnumerator ZoomOut(){
		//StopCoroutine ("ZoomIn");
		float t = 0;
		while (t <= 1) {
			mainCamera.fieldOfView = Mathf.Lerp (60f, 30f, t);
			t += .01f;
			yield return new WaitForSeconds (.01f);
			canZoom = true;
		}
	}
	IEnumerator Zoom(float [] range){
		float t = 0;
		while (t <= 1) {
			mainCamera.fieldOfView = Mathf.Lerp (range[0], range[1], t);
			t += .01f;
			yield return new WaitForSeconds (.01f);
			canZoom = true;
		}
	}
}
