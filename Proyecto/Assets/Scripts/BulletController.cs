﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {
	//decalGO prefab del decal a instanciar
	public GameObject decalGO;

	void OnCollisionEnter(Collision objectCollision){
		//decalI instancia del decal
		GameObject decalI;
		//con colision genera decal 
		if (objectCollision.gameObject.tag != "Decal" && objectCollision.gameObject.tag != "Enemy" && objectCollision.gameObject.tag != "Player") {
			decalI = Instantiate (decalGO,
									objectCollision.contacts [0].point + (objectCollision.contacts [0].normal * 0.1f), 
									Quaternion.LookRotation (-objectCollision.contacts [0].normal));
			decalI.transform.parent = objectCollision.gameObject.transform;
			Destroy (decalI, 5);
		}
		//destruye la bala
		Destroy(this.gameObject);


	}
}

