﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Loader : MonoBehaviour
{
	//instancia estatica
	public static Loader instance;
	//screen
	public Image loaderImage;
	public Text loaderText;
    //cargador
	AsyncOperation loaderScene;
	//indica si termino carga
    bool loading;
	//indica si tiene que destruirse
	bool destroy=false;


	void Awake(){
		instance = this;
	}

	void Start (){
		//para que no se destruya al cargar
        DontDestroyOnLoad(this.gameObject);
        //oculta e indica que no esta cargando
		loading = false;
		this.gameObject.SetActive(false);
	}

	void Update (){
		//si esta cargando
        if (loading)
        {
			//obtiene el progreso
			float loaded=loaderScene.progress;
			//cambia los textos por el progreso
			loaderText.text = "loading ..." + (loaded*100)+"%";
			//llena la barra segun el progreso
			loaderImage.fillAmount = loaded;
			//si ya cargo
			if (loaderScene.isDone) {
				//oculta la pantalla de carga
				this.gameObject.SetActive (false);
				if (destroy)
					Destroy (this.gameObject);
			}
        }
	}

	//es llamada cuando se quiere carga una escena
	public void LoadScene(int index)
    {
		//muestra la carga y comienza a cargar
		this.gameObject.SetActive(true);
		loaderScene = SceneManager.LoadSceneAsync(index);
        loading = true;
		if(index==0)
			destroy = true;
    }


}
