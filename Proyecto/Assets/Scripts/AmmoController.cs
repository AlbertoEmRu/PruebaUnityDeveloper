﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoController : MonoBehaviour {
	public int bullets = 10;
	//trigger con municion agrega balas al inventario
	void OnTriggerEnter(Collider objCollider){
		if (objCollider.transform.tag == "Player") {
			PlayerProfile.instance.GetAmmo (bullets);
			Destroy (this.gameObject);
		}
	}
}
