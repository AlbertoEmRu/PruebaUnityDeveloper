﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutomaticShooter : MonoBehaviour {
	//bala
	public GameObject bullet;
	//fuerza de disparo
	public float bulletForce=50f;
	//tiempo entre disparos
	public float time=2f;
	void Start(){
		InvokeRepeating ("Shoot", 10f, time);
	}
	void Shoot(){
		GameObject bulletInstance=Instantiate(	bullet, 
			transform.position+transform.forward,
			transform.rotation*bullet.transform.rotation);
		bulletInstance.GetComponent<Rigidbody>().AddForce(transform.forward * bulletForce, ForceMode.Impulse);
		Destroy(bulletInstance,2f);

	}
}