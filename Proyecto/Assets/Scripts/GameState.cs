using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//enum para los estados del juego
public enum States{PAUSE,PLAY}

public class GameState : MonoBehaviour {
	
	public static GameState instance;
	public States currentState;
	//Eventos y delegates
	public delegate void PauseEvent ();
	public PauseEvent pause;
	public delegate void PlayEvent ();
	public PlayEvent play;

	void Awake () {
		//instancia y subscripciones
		instance = this;
		pause += GamePause;
		play += GamePlay;
	}

	//se llama al cambiar de estado
	public void ChangeState(States newState){
		currentState = newState;
		switch (currentState) {
		case States.PAUSE:
				pause ();
				break;
			case States.PLAY:
				play ();
				break;

		}
	}

	//pausa
	public void GamePause (){
		Time.timeScale = 0;
	}
	//jugar
	public void GamePlay(){
		Time.timeScale = 1;
	}

}
