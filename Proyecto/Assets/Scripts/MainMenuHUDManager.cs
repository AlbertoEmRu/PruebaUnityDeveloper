﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuHUDManager : MonoBehaviour {
	public GameObject mainPanel;
	public GameObject highScorePanel;
	public GameObject levelPanel;
	public GameObject directionsPanel;

	//activa y desactiva los paneles de acuerdo a los paneles que se quieren mostrar
	public void ToMainPanel(){
		mainPanel.SetActive (true);
		highScorePanel.SetActive (false);
		levelPanel.SetActive (false);
		directionsPanel.SetActive (false);
	}
	public void ToHighScorePanel(){
		highScorePanel.SetActive (true);
		mainPanel.SetActive (false);
	}
	public void ToLevelPanel(){
		levelPanel.SetActive (true);
		mainPanel.SetActive (false);
	}
	public void ToDirectionsPanel(){
		directionsPanel.SetActive (true);
		mainPanel.SetActive (false);
	}

}
