﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBulletShooter : MonoBehaviour {
	//objeto a instanciar
	public GameObject bullet;
	//fuerza de disparo
	public float bulletForce;
	//rango de punteria
	public float aim=5f;
	//disparo con un fallo random para dar posibilidad de esquivar
	public void Shoot(){
		float rand = Random.Range (-aim, aim);
		transform.Rotate (0f, rand, 0f);
		GameObject bulletInstance = Instantiate (bullet, 
			                          transform.position + transform.forward,
			                          transform.rotation * bullet.transform.rotation);
		
		bulletInstance.GetComponent<Rigidbody> ().AddForce (transform.forward * bulletForce, ForceMode.Impulse);
		transform.Rotate (0f, -rand, 0f);
		Destroy (bulletInstance, 2f);
	}
}
