﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDManager : MonoBehaviour {
	public static HUDManager instance;
	//elementos a controlar en la interfaz de usuario (en juego)
	public Text bulletCounter;
	public Image lifeBar;
	public Text time;
	public Text kills;
	//elementos de game over
	public Text gameOverText;
	public Text gOKills;
	public Text gOTime;
	//paneles
	public GameObject pausePanel;
	public GameObject gameOverPanel;
	public GameObject highScorePanel;
	public GameObject newHighPanel;
	//inicializa en valores de 0
	void Awake(){
		instance = this;
		bulletCounter.text = "0/0";
		time.text="00:00";
		lifeBar.fillAmount = 1f;
		lifeBar.color = Color.green;
		pausePanel.SetActive (false);
		highScorePanel.SetActive (false);
		newHighPanel.SetActive (false);
		Cursor.visible = false;
	}

	//actualiza texto en cuenta de balas
	public void SetBulletCount(int count,int total){
		bulletCounter.text = count + "/" + total;
	}
	//actualiza el contador de tiempo
	public void SetTime(string count){
		time.text = count;
	}
	//actualiza barra de vida del jugador
	public void SetLife(float percent){
		StartCoroutine ("SetFillAmount",new float[]{lifeBar.fillAmount,percent} );
		lifeBar.color = Color.Lerp (Color.red, Color.green, percent);
	}
	//suaviza el vaciado de la barra de vida
	IEnumerator SetFillAmount(float[] limit){
		float t = 0.0f;
		while (t < 1) {
			t += 0.03f;
			lifeBar.fillAmount = Mathf.Lerp (limit[0],limit[1], t);
			yield return new WaitForSeconds (0.01f);
		}
	}
	//actualiza el numero de enemigos matados
	public void SetKills(int enemy){
		kills.text = enemy.ToString ();
	}
	//inicia el menu de pausa
	public void Pause(){
		pausePanel.SetActive (true);
		GameState.instance.ChangeState (States.PAUSE);
		Cursor.visible = true;
	}
	//regresa a la pantalla de juego
	public void PlayMode(){
		pausePanel.SetActive (false);
		GameState.instance.ChangeState (States.PLAY);
		Cursor.visible = false;
	}
	//game over
	public void GameOver(int type){
		if (type == 1) {
			gameOverText.text = "Nivel Completado";
		}else{
			gameOverText.text = "Fin del juego";
		}
		gameOverPanel.SetActive (true);

		Cursor.visible = true;
		GameState.instance.ChangeState (States.PAUSE);

		gOKills.text = PlayerProfile.instance.GetKills ().ToString ();
		gOTime.text = PlayerProfile.instance.GetTime ();
	}
	//highscores
	public void HighScores(){
		gameOverPanel.SetActive (false);
		highScorePanel.SetActive (true);
		pausePanel.SetActive (false);
		PlayerProfile.instance.SendHS ();
	}
	//to main menu
	public void ToMainMenu(){
		GameState.instance.ChangeState (States.PLAY);
		Loader.instance.LoadScene (0);
	}
}
