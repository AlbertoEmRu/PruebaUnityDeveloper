﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;


public enum EnemyState{	PATROL,CHASE,FIRE}

public class EnemyController : MonoBehaviour {
	//variables de patrulla
	public Vector2 patrolArea;
	public float timeToUpdatePatrol;
	public float minDistanceToPatrol;
	Vector3 startEnemyPosition;
	//variables de movimiento y estado
	EnemyState currentState;
	Vector3 destination;
	NavMeshAgent agent;
	//variables de combate
	public int fireDistance;
	public EnemyBulletShooter enemyBulletShooter;
	public int shootTime;
	public Vector2 combatArea;
	public float combatTime=3f;
	bool shoot=true;
	Ray playerRay;
	RaycastHit playerHit;
	//resistencia y daño de enemigo
	float hitCounter;
	int resistance;
	//jugador y camara
	GameObject player;
	Transform playerTransform;
	Transform cameraTransform;
	//barra de vida del enemigo
	public GameObject lifeCanvas;
	Image lifeBar;
	//objeto a soltar
	public GameObject ammo;
	//si es unico o con spawner
	public bool respawn=true;
	//inicializa los elementos y busca al juagador
	void Start () {
		
		agent = GetComponent<NavMeshAgent> ();
		player = GameObject.FindGameObjectWithTag ("Player");
		playerTransform = player.transform;

		hitCounter = 0;
		resistance = 5;

		currentState = EnemyState.PATROL;
		startEnemyPosition = transform.position;
		StartCoroutine ("UpdatePatrolDestination");

		cameraTransform = GameObject.FindGameObjectWithTag ("MainCamera").transform;
		lifeCanvas.transform.LookAt (cameraTransform);
		lifeBar=lifeCanvas.GetComponentInChildren<Image>();
		lifeBar.color = Color.green;

		enemyBulletShooter=GetComponentInChildren<EnemyBulletShooter>();
	}

	void Update(){
		//si el jugador esta mas lejos que la distancia de patrulla sigue patrullando
		if ((playerTransform.position - transform.position).sqrMagnitude > (minDistanceToPatrol * minDistanceToPatrol)) {
			if (currentState != EnemyState.PATROL)
				StartCoroutine ("UpdatePatrolDestination");
			currentState = EnemyState.PATROL;
		}
		//si el jugador esta entre distancia de tiro y patrullaje lo persigue
		else if ((playerTransform.position - transform.position).sqrMagnitude > fireDistance * fireDistance) {
			StopCoroutine ("UpdatePatrolDestination");
			currentState = EnemyState.CHASE;
			destination = playerTransform.position-(playerTransform.position-transform.position).normalized*5f;
		} 
		//si el jugador esta a distancia de tiro empieza a disparar
		else {
			if (currentState != EnemyState.FIRE) {
				StartCoroutine ("CombatMove");
				currentState = EnemyState.FIRE;
				destination = transform.position;
			}
			transform.LookAt (playerTransform);
		}
		//si el rayo da en el jugador dispara contra el
		playerRay = new Ray (transform.position, transform.forward);
		if (shoot) {
			if (Physics.Raycast (playerRay, out playerHit)) {
				if (playerHit.transform.tag == "Player") {
					enemyBulletShooter.Shoot ();
					shoot = false;
					StartCoroutine ("CoolDown");
				}
			}
		}
		//nueva direccion
		agent.SetDestination (destination);
		//barra siempre hacia la camara
		lifeCanvas.transform.LookAt (cameraTransform);
	}

	//permite volver a tirar cuando pase el tiempo de espera
	IEnumerator CoolDown(){
		yield return new WaitForSeconds (shootTime);
		shoot = true;
	}
	//genera un nuevo destino de patrullaje en un area
	IEnumerator UpdatePatrolDestination(){
		destination = new Vector3 (startEnemyPosition.x+ Random.Range(-patrolArea.x,patrolArea.x),
			startEnemyPosition.y,
			startEnemyPosition.z+Random.Range(-patrolArea.y,patrolArea.y));
		
		yield return new WaitForSeconds (timeToUpdatePatrol);
		if (currentState == EnemyState.PATROL)
			StartCoroutine( "UpdatePatrolDestination");

	}
	//se mueve en un area para evadir disparos
	IEnumerator CombatMove(){
		destination = destination + new Vector3 (   Random.Range (-combatArea.x, combatArea.x),
													0,
													Random.Range (-combatArea.y, combatArea.y));
		yield return new WaitForSeconds (combatTime);
		if(currentState ==EnemyState.FIRE)
			StartCoroutine ("CombatMove");
	}

	//si le da una bala sufre daño
	void OnCollisionEnter(Collision objCollision){
		if (objCollision.transform.tag == "Bullet"){
			hitCounter += 1;
			if (hitCounter == resistance) {
				PlayerProfile.instance.EnemyKill ();
				if(respawn)
					EnemySpawner.instance.EnemyDead ();
				Instantiate (ammo, transform.position, Quaternion.identity);
				Destroy (this.gameObject);
			}
			SetLife ((resistance-hitCounter)/resistance);
		}
	}
	//suaviza el cambio de vida
	public void SetLife(float percent){
		StartCoroutine ("SetFillAmount",new float[]{lifeBar.fillAmount,percent});
		lifeBar.color = Color.Lerp (Color.red, Color.green, percent);
	}
	IEnumerator SetFillAmount(float[] limit){
		float t = 0.0f;
		while (t < 1) {
			t += 0.03f;
			lifeBar.fillAmount = Mathf.Lerp (limit[0],limit[1], t);
			yield return new WaitForSeconds (0.01f);
		}
	}
}
