﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class HighScores : MonoBehaviour {
	//instancia
	public static HighScores instance;
	//elementos de la pantalla de highscore
	public GameObject[] register;
	public GameObject newHigh;
	public InputField newNameField;
	//informacion de puntajes anteriores
	int[] savedKills;
	string[] savedLevel;
	int[] savedTimeS;
	int[] savedTimeM;
	string[] savedName;
	//nuevo registro
	int newIndex;
	int newKills;
	string newLevel;
	int newTimeS;
	int newTimeM;
	string newName;
	//crea la instancia
	void Awake(){
		instance = this;
	}
	//lee los datos guardados y los carga en pantalla
	void Start () {
		savedKills = new int[5];
		savedLevel = new string[5];
		savedTimeS = new int[5];
		savedTimeM = new int[5];
		savedName = new string[5];

		for(int i =0; i<5 ;i++){
			savedKills[i]=PlayerPrefs.GetInt("Kills"+i.ToString());
			if (savedKills[i] != 0) {
				savedLevel [i] = PlayerPrefs.GetString ("Level" + i.ToString ());
				savedTimeS [i] = PlayerPrefs.GetInt ("TimeS" + i.ToString ());
				savedTimeM [i] = PlayerPrefs.GetInt ("TimeM" + i.ToString ());
				savedName [i] = PlayerPrefs.GetString ("Name" + i.ToString ());

				register [i].GetComponentInChildren<Text> ().text = "\t\t"+savedKills[i].ToString () + "\t\t\t\t\t" +
					savedTimeM [i].ToString ("D2") + ":" + savedTimeS [i].ToString ("D2") + "\t\t\t\t\t" + savedLevel [i]+ 
					"\t\t\t\t" + savedName [i];
				register [i].SetActive (true);
			} else
				register [i].SetActive (false);
		}
	}

	//verifica si es un highscore y en que posicion va
	public int CheckHS(){
		for (int i = 0; i <= 4; i++) {
			Debug.Log (savedKills + "  k  " + newKills);
			if (savedKills [i] > newKills)
				continue;
			else if (savedKills[i] == newKills) {
				Debug.Log(savedTimeM [i] +"  TM  "+ newTimeM);
				if (savedTimeM [i] < newTimeM) {
					continue;
				} else if (savedTimeM[i] == newTimeM) {
					Debug.Log (savedTimeS [i] + "  TS  " + newTimeS);
					if (savedTimeS [i] < newTimeS) {
						continue;
					}
				}
			} 
			return i;
		}
		return 5;
	}

	//activa la ventana para capturar nombre y verifica que lugar es
	public void NewHighScore(int kills,int timeM,int timeS,int level){
		newKills = kills;
		newTimeM = timeM;
		newTimeS = timeS;
		if (level == 1)
			newLevel = "Entr.";
		else
			newLevel = "Nv.1";
		newIndex = CheckHS ();
		if (newIndex <= 4 ) {
			newHigh.SetActive (true);
		} else
			newHigh.SetActive (false);
	}

	//al dar clic recorre los registros y guarda el nuevo registro
	public void Continue(){
		newName = newNameField.text;
		newHigh.SetActive (false);
		for (int i = 3; i >= newIndex; i--) {
			PlayerPrefs.SetInt ("Kills" + (i+1).ToString (),savedKills [i] );
			PlayerPrefs.SetString ("Level" + (i+1).ToString (),savedLevel [i] );
			PlayerPrefs.SetInt ("TimeS" + (i+1).ToString (),savedTimeS [i]);
			PlayerPrefs.SetInt ("TimeM" + (i+1).ToString (),savedTimeM [i]);
			PlayerPrefs.SetString ("Name" + (i+1).ToString (),savedName [i]);
			savedKills [i + 1] = savedKills [i];
			savedLevel [i + 1] = savedLevel [i];
			savedTimeS [i + 1] = savedTimeS [i];
			savedTimeM [i + 1] = savedTimeM [i];
			savedName [i + 1] = savedName [i];
			register [i+1].GetComponentInChildren<Text> ().text = "\t\t"+savedKills[i+1].ToString () + "\t\t\t\t\t" +
				savedTimeM [i+1].ToString ("D2") + ":" + savedTimeS [i+1].ToString ("D2") + "\t\t\t\t\t" + savedLevel [i+1] + "\t\t\t\t" + savedName [i+1];
			if(savedKills[i+1]>0)
				register [i + 1].SetActive (true); 
		}
		register [newIndex].GetComponentInChildren<Text> ().text = "\t\t"+newKills.ToString () + "\t\t\t\t\t" +
			newTimeM.ToString ("D2") + ":" + newTimeS.ToString ("D2") + "\t\t\t\t\t" + newLevel + "\t\t\t\t" + newName;
		PlayerPrefs.SetInt ("Kills" + newIndex.ToString (), newKills);
		PlayerPrefs.SetString ("Level" + newIndex.ToString (),newLevel);
		PlayerPrefs.SetInt ("TimeS" + newIndex.ToString (),newTimeS);
		PlayerPrefs.SetInt ("TimeM" + newIndex.ToString (),newTimeM);
		PlayerPrefs.SetString ("Name" + newIndex.ToString (),newName);
		register [newIndex].SetActive (true);
		PlayerPrefs.Save ();
	}


}
