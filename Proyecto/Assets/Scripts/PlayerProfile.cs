﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;

public class PlayerProfile : MonoBehaviour {
	//instancia estatica
	public static PlayerProfile instance;

	//informacion de la partida del jugador
	int kills=0;
	int damage=0;
	float resistance=5f;
	int bullets=10;
	public int totalBullets=20;
	int reload=10;
	float timeElapsed=0;
	public int sensibility=1;
	//genera la referencia a la instancia
	void Awake(){
		instance = this;
	}

	//inicializa el HUD con la informacion del jugador
	void Start () {
		HUDManager.instance.SetLife ((resistance - damage) / resistance);
		HUDManager.instance.SetBulletCount (bullets, totalBullets);
	}
	//lleva el tiempo de juego
	void Update(){
		timeElapsed += Time.deltaTime;
		float min =Mathf.Floor( timeElapsed / 60);
		int seg = Mathf.RoundToInt(timeElapsed % 60);
		HUDManager.instance.SetTime (string.Format ("{0:00}:{1:00}", min, seg));
	}

	//pierde una bala de inventario y actualiza en pantalla
	public void Shoot(){
		bullets -= 1;
		HUDManager.instance.SetBulletCount (bullets, totalBullets);
	}
	//verifica el tener balas
	public bool CanShoot(){
		if (bullets > 0)
			return true;
		return false;
	}
	//recarga y actualiza en pantalla
	public void Reload(){
		if (totalBullets > (reload-bullets)) {
			totalBullets -= reload - bullets;
			bullets = reload;
		} else {
			bullets += totalBullets;
			totalBullets = 0;
		}
		HUDManager.instance.SetBulletCount (bullets, totalBullets);
	}
	//indica si se puede recargar
	public bool CanReload(){
		if (totalBullets > 0 && bullets<reload)
			return true;
		return false;
	}
	//indica daño al jugador y actualiza en pantalla
	public void PlayerHit(){
		damage += 1;
		HUDManager.instance.SetLife ((resistance - damage) / resistance);
		if (resistance == damage) {
			HUDManager.instance.GameOver (0);
		}
	}
	//indica muerte de enemigo y actaliza en pantalla
	public void EnemyKill(){
		kills += 1;
		HUDManager.instance.SetKills (kills);
	}
	//aumenta la cuneta de bals y muestra en pantalla
	public void GetAmmo(int newBullets){
		totalBullets += newBullets;
		HUDManager.instance.SetBulletCount (bullets, totalBullets);
	}
	//regresa las muertes hechas por el jugador
	public int GetKills(){
		return kills;
	}
	public string GetTime(){
		return ((int)Mathf.Floor (timeElapsed / 60)).ToString("D2") + ":" + Mathf.RoundToInt (timeElapsed % 60).ToString("D2");
	}

	public void SendHS(){
		HighScores.instance.NewHighScore(kills,
			(int)Mathf.Floor( timeElapsed / 60),
			Mathf.RoundToInt(timeElapsed % 60),
			SceneManager.GetActiveScene().buildIndex);
	}

	public void ChangeSensibility(float newValue){
		sensibility = (int)newValue;
	}
}
