﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullseye : MonoBehaviour {
	//si colisiona indica muerte y se destruye
	void OnCollisionEnter(Collision objCollision){
		if (objCollision.gameObject.tag == "Bullet")
			PlayerProfile.instance.EnemyKill ();
			Destroy (this.gameObject);
	}

}
