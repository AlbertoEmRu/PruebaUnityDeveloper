﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Win : MonoBehaviour {
	public int killObjetive;
	bool pause=true;
	//verifica que se tengan las muertes para ganar
	void Update () {
		if (pause & killObjetive == PlayerProfile.instance.GetKills ()) {
			pause=false;
			HUDManager.instance.GameOver (1);
		}
	}
}
