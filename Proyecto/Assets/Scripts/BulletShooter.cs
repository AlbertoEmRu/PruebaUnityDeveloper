﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletShooter : MonoBehaviour {
	//bala
	public GameObject bullet;
	//fuerza de disparo
	public float bulletForce=50f;
	//rayos de crosshair
	Ray crossHairRay;
	RaycastHit cHRayHit;
	public GameObject crossHair;

	//comportamiento de disparo
	void Update () {
		if (GameState.instance.currentState != States.PAUSE) {
			//dispara si tiene balas
			if (Input.GetMouseButtonDown (0) && PlayerProfile.instance.CanShoot ()) {
				GameObject bulletInstance = Instantiate (bullet, 
					                          transform.position + transform.forward,
					                          transform.rotation * bullet.transform.rotation);
				bulletInstance.GetComponent<Rigidbody> ().AddForce (transform.forward * bulletForce, ForceMode.Impulse);
				Destroy (bulletInstance, 2f);
				PlayerProfile.instance.Shoot ();
			}
			//recarga si aun tiene municion
			if (Input.GetMouseButtonDown (2) && PlayerProfile.instance.CanReload ()) {
				PlayerProfile.instance.Reload ();
			}
			//lanza rayo para colocar crosshair como ayuda
			crossHairRay = new Ray (transform.position, transform.forward);
			if (Physics.Raycast (crossHairRay, out cHRayHit)) {
				if (cHRayHit.transform.tag != "Bullet") {
					crossHair.transform.position = cHRayHit.point + (cHRayHit.normal * .01f);
					crossHair.transform.rotation = Quaternion.LookRotation (-cHRayHit.normal);
				}
			} else {
				crossHair.transform.position = transform.position - transform.forward;
			}
		}
	}
}
